package bellman_ford;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import dijkstra.Edge;
import dijkstra.GraphStructure;

public class BellmanFord {

	public static final String path = "/home/slyboots/development/workspace/air/Lab1/resource/data";
	static int nodes;
	static int edges;

	private static int[][] generateFullGraph(int nodes) {
		edges = nodes * (nodes - 1);
		int graph[][] = new int[edges][3];

		Random rand = new Random();

		List<List<Edge>> edgesList = new ArrayList<List<Edge>>();
		int edgesCounter = 0;
		for (int i = 0; i < nodes; i++) {
			for (int j = 0; j < nodes; j++) {
				if (i != j) {
					graph[edgesCounter][0] = i;
					graph[edgesCounter][1] = j;
					graph[edgesCounter][2] = Math.abs(rand.nextInt(100));
				}
			}

		}
		return graph;
	}

	public static void main(String[] args) {

		// graph
		// int graph[][]=FileMaintenance.loadData(path);
		for (int k = 1; k < 5; k++) {
			nodes=k*2000;
			int graph[][]=generateFullGraph(k*2000);
			Instant start = Instant.now();

			int results[] = new int[nodes];
			results[0] = 0;
			for (int i = 1; i < nodes; i++) {
				results[i] = Integer.MAX_VALUE;
			}
			int edge[];
			int min1;
			int min2;
			for (int i = 0; i < edges; i++) {
				edge = graph[i];
				min1 = results[edge[1]];
				min2 = results[edge[0]] + edge[2];
				results[edge[1]] = Integer.min(min1, min2);

			}
			Instant end = Instant.now();
			System.out.println(k*2000+" "+Duration.between(start, end).toMillis());
		}
		System.out.println("result");
		/*for (int i = 0; i < nodes; i++) {
			System.out.println(results[i]);
		}*/
	}

}
