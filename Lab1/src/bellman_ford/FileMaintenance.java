package bellman_ford;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileMaintenance {

public static int [][] loadData(String path){
		
		File file = new File(path);
		Scanner scanner = null;
		try {
			scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String graphSize=scanner.nextLine();
		String[] size = graphSize.split(" ");
		 BellmanFord.nodes=Integer.parseInt(size[0]);
		 BellmanFord.edges=Integer.parseInt(size[1]);
		int graph [][] = new int [BellmanFord.edges][3];
		int i=0;
		String row;
		String []rowsSplited;
		while(scanner.hasNext()){
			row=scanner.nextLine();
			rowsSplited=row.split(" ");
			graph[i][0]=Integer.parseInt(rowsSplited[0]);
			graph[i][1]=Integer.parseInt(rowsSplited[1]);
			graph[i][2]=Integer.parseInt(rowsSplited[2]);
			i++;
		}
		
	
		return graph;
		
		
	}
}
