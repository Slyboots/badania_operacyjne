package dijkstra;

public class Node {
	int nodeId;
	int cost;
	public Node(){
		
	}
	public Node(int nodeId,int cost){
		this.nodeId=nodeId;
		this.cost=cost;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cost;
		result = prime * result + nodeId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (cost != other.cost)
			return false;
		if (nodeId != other.nodeId)
			return false;
		return true;
	}

}
