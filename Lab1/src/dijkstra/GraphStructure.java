package dijkstra;

import java.util.List;

public class GraphStructure {

	int nodes;
	int edges;
	List<List<Edge>> edgesList;
	
	public GraphStructure(){
		
	}
	public GraphStructure(int nodes,int edges,List<List<Edge>> edgesList){
		this.nodes=nodes;
		this.edges=edges;
		this.edgesList=edgesList;
	}
}
