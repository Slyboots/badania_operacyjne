package dijkstra;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

public class Dijkstra {

	public static final String path = "/home/slyboots/development/workspace/air/Lab1/resource/data";

	private static GraphStructure generateFullGraph(int nodes) {
		GraphStructure graph = new GraphStructure();
		graph.nodes = nodes;
		Random rand = new Random();

		List<List<Edge>> edgesList = new ArrayList<List<Edge>>();
		for (int i = 0; i < nodes; i++) {
			List<Edge> edges = new ArrayList<Edge>();
			for (int j = 0; j < nodes; j++) {
				if (i!=j) {
					edges.add(new Edge(i, j, Math.abs(rand.nextInt(100))));
				}
				
			}
			edgesList.add(edges);

		}
		graph.edgesList=edgesList;
		return graph;
	}

	public static void main(String[] args) {
		// load data from file
		// GraphStructure graph = FileMaintenance.loadData(path);
		// init
		for (int k = 1; k < 10; k++) {
			GraphStructure graph =generateFullGraph(k*2000);
			Instant start = Instant.now();

			List<Node> nodesCost = new ArrayList<Node>();
			int result[] = new int[graph.nodes];
			nodesCost.add(new Node(0, 0));
			result[0] = 0;
			for (int i = 1; i < graph.nodes; i++) {
				nodesCost.add(new Node(i, Integer.MAX_VALUE));
				result[i] = Integer.MAX_VALUE;
			}
			//
			Queue<Node> nodePriorityQueue = new PriorityQueue<Node>(new NodeComparator());
			for (Node node : nodesCost) {
				nodePriorityQueue.add(node);
			}

			Node nodeMin;
			Node relaxNode;
			Edge edge;
			while (!nodePriorityQueue.isEmpty()) {
				nodeMin = nodePriorityQueue.poll();
				List<Edge> adjList = graph.edgesList.get(nodeMin.nodeId);
				for (int i = 0; i < adjList.size(); i++) {
					edge = adjList.get(i);
					relaxNode = new Node(edge.to, result[edge.to]);
					nodePriorityQueue.remove(relaxNode);
					relaxNode.cost = Math.min(nodeMin.cost + edge.weight, relaxNode.cost);
					if(result[edge.to]!=relaxNode.cost){
						result[edge.to] = relaxNode.cost;
						nodePriorityQueue.add(relaxNode);
					}
					
				}
			}

			Instant end = Instant.now();
			System.out.println(k*2000+" "+Duration.between(start, end).toMillis()); // prints
																			// PT1M3.553S
		}
		/*
		 * for(int i=0;i<result.length;i++){
		 * System.out.println("nodeId "+i+" cost "+result[i]); }
		 */
	}

}
