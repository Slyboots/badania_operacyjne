package dijkstra;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileMaintenance {

public static GraphStructure loadData(String path){
		
		GraphStructure graph= new GraphStructure();
		File file = new File(path);
		Scanner scanner = null;
		try {
			scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String graphSize=scanner.nextLine();
		String[] size = graphSize.split(" ");
		graph.nodes=Integer.parseInt(size[0]);
		graph.edges=Integer.parseInt(size[1]);
		List<List<Edge>> edgesList= new ArrayList<List<Edge>>();
		for(int i=0;i<graph.edges;i++){
			edgesList.add(new ArrayList<Edge>());
		}
		String row;
		String []rowsSplited;
		while(scanner.hasNext()){
			row=scanner.nextLine();
			rowsSplited=row.split(" ");
			Edge edge= new Edge();
			edge.from=Integer.parseInt(rowsSplited[0]);
			edge.to=Integer.parseInt(rowsSplited[1]);
			edge.weight=Integer.parseInt(rowsSplited[2]);
			edgesList.get(edge.from).add(edge);
		}
		graph.edgesList=edgesList;
	
		return graph;
		
		
	}
}
