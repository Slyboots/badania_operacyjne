import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MaxFlow {

	private static int [][] generateExampleGraph(){
		int [][] graph = new int[6][6];
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				graph[i][j]=0;
			}
		}
		graph[0][1]=16;
		graph[0][2]=13;
		graph[1][3]=12;
		graph[2][4]=14;
		graph[3][5]=20;
		graph[4][3]=7;
		graph[4][5]=4;
		return graph;
		
	}
	public static void main(String[] args) {
		
		int [][] graph=generateExampleGraph();
		int[][] result = EdmondsaKarpa(graph, 6, 0, 5);
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				if(result[i][j]>0)
					System.out.println(i+" --> "+j+"  "+result[i][j]);
			}
		}
	}
	
	private static int [][] EdmondsaKarpa(int [][] graph,int graphSize,int source,int sink){
	
	
		int resultFlow[][]= new int[graphSize][graphSize];
		int residualGraph[][]= new int[graphSize][graphSize];
		//inicjalizacja przeplywu
		for(int i=0;i<graphSize;i++){
			for(int j=0;j<graphSize;j++){
				resultFlow[i][j]=0;//poczatkowy flow=0
				residualGraph[i][j]=graph[i][j];
			}
		}
		
		//wykonujemy dopoki istnieja sciezki powiekszajace
		List<Integer> path = bfs(residualGraph,graphSize,source,sink);
		int min;
		int currentFlow;
		while(path.size()>0){
			min=Integer.MAX_VALUE;
			for(int j=0;j<path.size()-1;j++){
				currentFlow=residualGraph[path.get(j+1)][path.get(j)];
				min=Math.min(min, currentFlow);
			}
			for(int j=0;j<path.size()-1;j++){
				if(graph[path.get(j+1)][path.get(j)]>0){
					resultFlow[path.get(j+1)][path.get(j)]+=min;
				}
				else{
					resultFlow[path.get(j+1)][path.get(j)]-=min;
				}
				residualGraph[path.get(j+1)][path.get(j)]-=min;
				residualGraph[path.get(j)][path.get(j+1)]+=min;
			}
			path = bfs(residualGraph,graphSize,source,sink);
		}
		return resultFlow;
	}
	
	private static List<Integer> bfs(int [][]graph,int graphSize,int source,int sink){
		Node []nodes= new Node[graphSize];
		for(int i=0;i<graphSize;i++){
			nodes[i]= new Node(i,0,-1);
		}
		nodes[source].color=1;
		nodes[source].prev=0;

        LinkedList<Integer> fifo = new LinkedList<Integer>();

		//kolejka fifo do przeszukiwania grafu w szerz
        fifo.add(0);
		int currentNode;
		while(!fifo.isEmpty()){
			currentNode=fifo.remove();
			for(int i=0;i<graphSize;i++){
				if(graph[currentNode][i]>0){
					if(nodes[i].color==0){
						nodes[i].color=1;
						nodes[i].pathDis=nodes[currentNode].pathDis+1;
						nodes[i].prev=currentNode;
						fifo.add(i);
					}
				}
			}
			nodes[currentNode].color=2;
		}
		
	
		return pathFromSink(nodes,graphSize,source,sink);
	}
	
	private static List<Integer> pathFromSink(Node []nodes,int graphSize,int source,int sink){
		List<Integer> result = new ArrayList<Integer>();
		Node currentNode=nodes[sink];
		for(int i=0;i<graphSize;i++){
			result.add(currentNode.index);
			if(currentNode.prev<0){
				return  new ArrayList<Integer>();
				//sciezka nie istnieje zwracam pusta liste
			}
			currentNode=nodes[currentNode.prev];
			if(currentNode.index==source){
				result.add(currentNode.index);
				break;
			}
		}
		return result;
	}
}
