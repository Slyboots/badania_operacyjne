
public class Node {

	int index;
	int color;
	int prev;
	int pathDis;
	public Node(int index, int color, int prev) {
		super();
		this.index = index;
		this.color = color;
		this.prev = prev;
		this.pathDis=0;
	}
}

